'use strict'
/**
 * @mysql.js
 * This file allows loading the mysql NPM node module.
 * Credentials are for a local testing mysql server
 */

let mysql = require('mysql')
let connection = mysql.createConnection({
  host: '10.0.0.41',
  user: 'root',
  password: 'password',
  database: 'coinvac_cv'
})
let beginConnection = () => {
  let sqlPromise = new Promise((resolve, reject) => {
    connection.connect(function (err) {
      if (err) {
        logger.error(`Error connection to SQL Database; err: ${err}`)
        resolve(false)
      } else {
        logger.info(`Connected to SQL Database; connectionID: ${connection.threadId}`)
        resolve(true)
      }
    })
  })
  return sqlPromise
}
let executeSQLQuery = (qry) => {
  let sqlPromise = new Promise((resolve, reject) => {
    connection.query(qry, function (error, results, fields) {
      if (error) {
        reject(error)
      } else {
        let result = {results: results, fields: fields}
        resolve(result)
      }
    })
  })
  return sqlPromise
}
let getLastInsertID = () => {
  let sqlPromise = new Promise((resolve, reject) => {
    let idQuery = `SELECT * FROM coins ORDER BY auction_id DESC LIMIT 0, 1`
    let sqlQryPromise = executeSQLQuery(idQuery)
    sqlQryPromise.then((val) => {
      if (val.results) {
        let auction_id = val.results[0].auction_id
        resolve(auction_id)
      } else {
        reject('Something went wrong gettting last insert ID')
      }
    })
    .catch((err) => {
      reject(err)
    })
  })
  return sqlPromise
}
let insertCoin = (coinRecord) => {
  let sqlPromise = new Promise((resolve, reject) => {
    let sqlStatement = 'INSERT INTO coins SET ?'
    let qry = {sql: sqlStatement, values: coinRecord}
    let sqlQryPromise = executeSQLQuery(qry)
    sqlQryPromise.then((val) => {
      if (val.results) {
        let auction_id = val.results.insertId
        resolve(auction_id)
      } else {
        reject(`Something went wrong trying to insert ${coinRecord}`)
      }
    })
    .catch((err) => {
      reject(`Something went wrong trying to insert; err: ${err}`)
    })
  })
  return sqlPromise
}
let checkIfUnique = (auction_title) => {
  let sqlPromise = new Promise((resolve, reject) => {
    let sqlStatement = 'Select * FROM finds_unique_coin WHERE ?'
    let qry = {sql: sqlStatement, values: [{title: auction_title}]}
    let sqlQryPromise = executeSQLQuery(qry)
    sqlQryPromise.then((val) => {
      if (val.results && val.results.length === 0) {
        logger.info(`Auction Title is unique: ${auction_title}`)
        resolve(true)
      } else {
        resolve(false)
      }
    })
    .catch((err) => {
      reject(`Something went wrong trying to check if auction_title is unique ${auction_title}; err: ${err}`)
    })
  })
  return sqlPromise
}
let insertUniqueRecord = (auction_title) => {
  let sqlPromise = new Promise((resolve, reject) => {
    let sqlStatement = 'INSERT INTO finds_unique_coin SET ?'
    let qry = {sql: sqlStatement, values: [{title: auction_title}]}
    let sqlQryPromise = executeSQLQuery(qry)
    sqlQryPromise.then((val) => {
      if (val.results) {
        resolve(auction_title)
      } else {
        reject(`Something went wrong trying to insert into finds_unique_coin with: ${auction_title}`)
      }
    })
    .catch((err) => {
      reject(`Something went wrong trying to insert into finds_unique_coin with: ${auction_title}; err: ${err}`)
    })
  })
  return sqlPromise
}

// SELECT * FROM permlog ORDER BY id DESC LIMIT 0, 1

window.SQL = {
  connection: connection,
  beginConnection: beginConnection,
  executeSQLQuery: executeSQLQuery,
  getLastInsertID: getLastInsertID,
  insertCoin: insertCoin,
  checkIfUnique: checkIfUnique,
  insertUniqueRecord: insertUniqueRecord
}

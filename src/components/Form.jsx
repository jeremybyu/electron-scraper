'use strict'

let React = require('react')

let Form = React.createClass({
  propTypes: {
    processingRecords: React.PropTypes.bool.isRequired,
    eStop: React.PropTypes.bool.isRequired,
    submitHandler: React.PropTypes.func.isRequired,
    stopHandler: React.PropTypes.func.isRequired
  },
  getInitialState () {
    return {
    }
  },
  render () {
    let content = (
      <button name='submit' className='button button-outline'>
         Send
       </button>
    )
    if (this.props.processingRecords) {
      content = (
      <div>
        <button disabled name='submit' className='button button-outline'>
          Processing <i className='fa fa-spinner fa-spin'></i>
        </button>
        <div className='float-right'>
          <button onClick={this.props.stopHandler} name='stop' className='button-red button-outline'>
            Stop {this.props.eStop ? <i className='fa fa-spinner fa-spin'></i> : ''}
          </button>
        </div>
      </div>
      )
    }
    return (
    <div>
      <form ref='form' id='form' onSubmit={this.props.submitHandler}>
        <fieldset>
          <label htmlFor='urlField'>URL</label>
          <input type='text' placeholder='google.com' id='urlField'/>
          <label htmlFor='venueField'>Venue</label>
          <input style={{display: 'inline-block'}} type='number' placeholder='99999' id='venueField'/>
          <label htmlFor='categoryField'>Subcategory</label>
          <input style={{display: 'inline-block'}} type='number' placeholder='99999' id='categoryField'/>
          {content}
        </fieldset>
      </form>
    </div>
    )
  }
})
module.exports = Form

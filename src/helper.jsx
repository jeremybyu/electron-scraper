/* global fetch */
require('babel-polyfill')
let fakeID = 0
function replaceAll (starting, search, replacement) {
  return starting.replace(new RegExp(search, 'g'), replacement)
}
export async function getJSON (url) {
  let response = await fetch(url)
  let data = await response.json()
  return data
}
export function getUniqueID () {
  let newID = new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve(++fakeID)
    }, 5000)
  })
  return newID
}
export function mapData (record, venue, subcategory) {
  let newRecord = {}

  newRecord.pasID = record.id
  newRecord.auction_id = null
  newRecord.venue = venue
  newRecord.auction_subcategory = subcategory
  newRecord.auction_title = record.old_findID
  newRecord.auction_date = new Date(Date.parse(record.created))
  newRecord.auction_image = ''
  //  Remove Paragraph Markups
  let tempDesc = replaceAll(record.description, '</p>', '<br>')
  tempDesc = replaceAll(tempDesc, '<p>', '')
  tempDesc = replaceAll(tempDesc, `\n\n`, '<br>')
  //  logger.info(tempDesc)
  newRecord.auction_description = `Object type: ${record.objecttype}<br>` +
  `Broad period: ${record.broadperiod}<br>` +
  `County: ${record.county}<br>` +
  `${tempDesc}`
  //  logger.info(newRecord.auction_description)
  return newRecord
}
// $url = $_POST['url'];
// $title = $coin['old_findID'];
// $desc = 'Object type: '.$coin['objecttype'].'<br>';
// $desc .= 'Broad period: '.$coin['broadperiod'].'<br>';
// $desc .= 'County: '.$coin['county'].'<br>';
// $desc .= $coin['description'];
// $created_date = date('Y-m-d',strtotime($coin['created']));
// $image = 'http://finds.org.uk/'.$coin['imagedir'].$coin['filename'];
// $img_filename = $coin['filename'];
// //check for duplicate coin
//
// $id = get_next_coin_id(); //Looking at the database, I’m guessing this is an incrementing #. How do I get this info?
//
//
// $insertArr = array(
//     'auction_id' => $id,
//     'venue' => $venue,
//     'auction_subcategory' => $subcategory,
//     'auction_title' => $title,
//     'auction_description' => $desc,
//     'auction_date' => $created_date
//     );

'use strict'
//  Need babel polyfill to use es7 features, async functions
require('babel-polyfill')

var React = require('react')
var ReactDom = require('react-dom')
let Form = require('./components/Form.jsx')
let SQLConn = require('./components/SQLConn.jsx')

//  Access to global variables here.  This allows the web environment to use NPM node server modules
let Utils = window.Utils
let SQL = window.SQL
import * as AsyncUtils from './helper.jsx'

const DOCSPERPAGE = 20

var App = React.createClass({
  getInitialState () {
    return {
      totalRecords: 0, //  The Total amount of records to be processed
      currentRecord: {}, //  Current Record being processed
      recordCounter: 0,
      processingRecords: false, //  Are we currently processing
      eStop: false, //  Emergency stop on processing,
      pageProcessing: 0
    }
  },
  handleSubmit (event) {
    event.preventDefault()
    logger.info(`Submit Event`)
    let url = Utils.removeLastSlash(event.target.urlField.value)
    let venue = event.target.venueField.value
    let category = event.target.categoryField.value
    //  TODO verify url quality, client specified not necessary...
    var result = new Promise((resolve, reject) => {
      Utils.queryNumRecords(url, resolve, reject)
    })
    //  This first result should return how many total records wer are going to need to process.
    //  Once thats done we will need to continue to extract.

    this.setState({processingRecords: true})
    result.then((val) => {
      this.setState({totalRecords: val, processingRecords: true})
      logger.info(`Total Records: ${val}`)
      //  Begin asynchronous scraping
      this.beginScraping(url, venue, category)
    }).catch((err) => {
      logger.error(err)
      this.setState({processingRecords: false})
    })
  },
  handleStop (event) {
    event.preventDefault()
    this.setState({eStop: true})
  },
  //  This function performs the scraping of the requested website, all done asynchronously
  async beginScraping (baseURL, venue, category) {
    let numPages = Math.floor(this.state.totalRecords / DOCSPERPAGE) + 1 // Get number of pages to process
    logger.info(`Processing ${numPages} Pages`)
    //  Cycle through each page available.  Each page will contain 20 records by default
    for (let i = 1; i <= numPages && !this.state.eStop; i++) {
      let newJsonUrl = Utils.newJsonUrl(Utils.newURL(baseURL, i))
      logger.info(`New Page URL ${newJsonUrl}`)
      try {
        let coinList = await AsyncUtils.getJSON(newJsonUrl) // Get JSON Data for single page
        let reducedCoinList = coinList.results.map((item) => AsyncUtils.mapData(item, venue, category)) // Aggregrate and simplify the data
        logger.info(`List of 20 records found: ${reducedCoinList}`)
        //  Cycle through each of records retrieved by the page
        for (let record of reducedCoinList) {
          this.setState({currentRecord: record, recordCounter: ++this.state.recordCounter}) //  Update state for react UI
          logger.info(`Processing record: ${JSON.stringify(record)}`)
          // Check if coin is unique by querying the finds_unique_coin Database
          try {
            let isUnique = await SQL.checkIfUnique(record.auction_title)
            if (!isUnique) {
              logger.info(`Record is not unique. Record: ${JSON.stringify(record)}`)
              continue  //  Skip this record, nothing below will be exceuted in the for statement
            }
          } catch (err) {
            logger.info(`${err}`)
            continue  // Might need a break here
          }
          //  Get Unique ID from SQL Query
          try {
            let lastID = await SQL.getLastInsertID() //  Get unique ID from server for this new record
            let uniqueID = ++lastID
            record.auction_id = uniqueID
            //  Scrape Website Images, can not find all images in JSON request, must manually scrape
            try {
              let imgFiles = await Utils.scrapeImageFiles(uniqueID, record.pasID) //  Scrape Image files needed for this record
              logger.info(`Image Files : ${imgFiles}`)

              let finalFileNames = await Utils.downloadImageFiles(uniqueID, imgFiles) //  Download Image Files, return array of file names
              record.auction_image = finalFileNames.toString()
              delete record.pasID //  PasID is for client program only, should not be in insertion record
              // Insert coin record into MySQL database into tables coin and finds_unique_coin
              try {
                let newCoinInsertID = await SQL.insertCoin(record)
                let newFUCInsertID = await SQL.insertUniqueRecord(record.auction_title)
                logger.info(`Inserted record: ${JSON.stringify(record)} into database with auction_id of ${newCoinInsertID} and title of ${newFUCInsertID}`)
              } catch (e) {
                logger.error(`Error inserting record into database: ${e}`)
              }
            } catch (e) {
              logger.error(`Error processing record: ${JSON.stringify(record)}; error: ${e}`)
            }
          } catch (e) {
            logger.error(`Error processing record: ${JSON.stringify(record)}; error: ${e}`)
          }
          // Break out of loop if stop is pressed.
          if (this.state.eStop) {
            break
          }
        }
      } catch (err) {
        logger.error(`Error processing page#: ${i}; error: ${err}`)
        continue
      }
    }
    this.setState({processingRecords: false})
  },
  getPercentComplete () {
    if (this.state.totalRecords === 0) {
      return 0
    }
    let p = Math.floor(this.state.recordCounter / this.state.totalRecords * 100)
    isNaN(p) ? p = 0 : p = p
    return p
  },
  getPercentClassName () {
    let className = `c100 p${this.getPercentComplete()}`
    return className
  },
  render () {
    return (
      <div className='container'>
        <div className='row'>
          <div className='column'>
            <h3>Coin Scraper</h3>
          </div>
          <div className='column'>
            <div style={{paddingTop: '10px'}} className='float-right'>
              <SQLConn/>
            </div>
          </div>
        </div>

        <div className='row'>
          <div className='column'>
            <Form processingRecords={this.state.processingRecords} eStop={this.state.eStop} submitHandler={this.handleSubmit} stopHandler={this.handleStop}/>
          </div>
        </div>
        <div className='row'>
          <div className='column'>
            Total Records Found: {this.state.totalRecords}
            <br/>
            Processing Record: {this.state.currentRecord.auction_title}
          </div>
          <div className='column'>
            <div className={this.getPercentClassName()}>
              <span>{this.getPercentComplete()}%</span>
              <div className='slice'>
                <div className='bar'></div>
                <div className='fill'></div>
              </div>
            </div>
            Processed: {this.state.recordCounter}/{this.state.totalRecords}
          </div>
        </div>
      </div>
    )
  }
})

ReactDom.render(< App />, document.getElementById('react-root'))

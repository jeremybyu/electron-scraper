var webpack = require('webpack')
var path = require('path')

var config = {
  context: path.resolve(__dirname, 'src/'),
  entry: './entry.jsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build')
  },
  target: 'atom',
  module: {
    loaders: [
      {
        test: /\.jsx?/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        loader: 'babel'
      },
      { test: /\.json$/, loader: 'json-loader' }
    ]
  }
}

module.exports = config

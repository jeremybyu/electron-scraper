'use strict'
/**
 * @scraper.js
 * This file allows loading NPM node modules such as X-ray and Download
 * These modules, and corresponding helper function are stored in the global variable Utils
 */

let Xray = require('x-ray')
let Download = require('download')
let fs = require('fs')

let x = Xray()
const DIRECTORY_COUNT = 10000
const IMAGE_DIRECTORY = './images/'

try {
  fs.statSync(IMAGE_DIRECTORY)
} catch (e) {
  fs.mkdirSync(IMAGE_DIRECTORY)
}
let replaceAll = (starting, search, replacement) => {
  return starting.replace(new RegExp(search, 'g'), replacement)
}
let removeLastSlash = (item) => {
  let index1 = item.lastIndexOf('/')
  let index2 = item.length - 1
  if (index1 === index2) {
    return item.substring(0, item.length - 1)
  } else {
    return item
  }
}

let queryTitle = (url, resolve, reject) => {
  x(url, 'title')((err, title) => {
    if (err) {
      reject(err)
    } else {
      resolve(title)
    }
  })
}
function pad (num, size) {
  var s = num + ''
  while (s.length < size) s = '0' + s
  return s
}
let cleanImgUrl = (imgUrl) => {
  let newURL = imgUrl.replace('medium/', '')
  return replaceAll(newURL, ' ', '%20') // Clean URL that have spaces in them
}
let isEmpty = (object) => {
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      return false
    }
  }
  return true
}
let scrapeImageFiles = (uniqueID, pasID) => {
  let imgFiles = new Promise((resolve, reject) => {
    let url = recordURL(pasID)
    x(url, {
      image1: '#imagepane > img@src',
      image2: '#imagepane > div:nth-child(3) > a@href'
    })((err, obj) => { // obj will return with one or two properties (image1,image2) depending if their are 2 images on page
      if (err) {
        reject(`Couldn't get records from uniqueID: ${uniqueID} and pasID: ${pasID} err: ${err}`)
      } else {
        let files = [] // Empty files array
        if (obj || !isEmpty(obj)) { //  Ensure obj is defined
          files.push(cleanImgUrl(obj.image1)) //  Clean first image and add to files array
          if (obj.hasOwnProperty('image2')) { //  Check that image2 exits, if it does add the url to the files array
            files.push(cleanImgUrl(obj.image2))
          }
          resolve(files)
        } else {
          reject(`Couldn't get records from uniqueID: ${uniqueID} and pasID: ${pasID}`)
        }
      }
    })
  // setTimeout(function () {
  //   resolve(1)
  // }, 5000)
  })
  return imgFiles
}
let newURL = (baseURL, pageNumber) => {
  return `${baseURL}/page/${pageNumber}`
}
let newJsonUrl = (baseURL) => {
  return `${baseURL}/format/json`
}
let recordURL = (id) => {
  return `https://finds.org.uk/database/artefacts/record/id/${id}`
}
let queryNumRecords = (url, resolve, reject) => {
  x(url, '#resultsSidebar > p:nth-child(8)')((err, val) => {
    if (err) {
      reject(err)
    } else {
      let cleanVal = val.replace(',', '') //  Remove ',' in number
      let myExp = /.*: (\d*).*/ //  Regular Expression to match number
      let matches = cleanVal.match(myExp) //  Execute regular expression
      if (matches !== null) {
        let value = matches[1]
        resolve(Number(value))
      } else {
        reject(`Cant find total record numbers: ${val}`)
      }
    }
  })
}
let getImgDirectory = (uniqueID) => {
  let newDir = Math.floor(uniqueID / DIRECTORY_COUNT)
  newDir = newDir * DIRECTORY_COUNT
  if (newDir === 0) {
    newDir = '00000'
  }
  let pathToSave = `${IMAGE_DIRECTORY}${newDir}/`
  try {
    fs.statSync(pathToSave)
  } catch (e) {
    fs.mkdirSync(pathToSave)
  }
  return pathToSave
}
let getFileExt = (url) => {
  return url.split('.').pop().toLowerCase()
}
let genFileName = (id, ext, num) => {
  num === undefined ? num = '' : num = num
  return `${pad(id, 5)}_${num}.${ext}`
}
let downloadImageFiles = (uniqueID, imgFiles) => {
  let destDir = getImgDirectory(uniqueID)
  let imgPromise = new Promise((resolve, reject) => {
    let imgNames = [] //  This will be the return array that contains the new file names for the images
    imgNames.push(genFileName(uniqueID, getFileExt(imgFiles[0]))) //  add the first image name e.g. 100001_.jpeg

    if (imgFiles.length > 1) {
      imgNames.push(genFileName(uniqueID, getFileExt(imgFiles[1]), 1)) //  add the second image name e.g. 100001_1.jpeg
      let d1 = new Download({mode: '755'})
      d1.get(imgFiles[0])
        .dest(destDir)
        .rename(imgNames[0])
        .run((err, files) => {
          if (err) {
            reject(`Error Downloading images for uniqueID: ${uniqueID}`)
          } else {
            logger.info(`Finished downloading Image 1 for uniuqeID: ${uniqueID}`)
            //  Will wait for next donwnload to finish before resovling promise
          }
        })
      let d2 = new Download({mode: '755'})
      d2.get(imgFiles[1])
        .dest(destDir)
        .rename(imgNames[1])
        .run((err, files) => {
          if (err) {
            reject(`Error Downloading images for uniqueID: ${uniqueID}`)
          } else {
            resolve(imgNames)
          }
        })
    } else {
      let d1 = new Download({mode: '755'})
      d1.get(imgFiles[0])
        .dest(destDir)
        .rename(imgNames[0])
        .run((err, files) => {
          if (err) {
            reject(`Error Downloading images for uniqueID: ${uniqueID}`)
          } else {
            resolve(imgNames)
          }
        })
    }
  })
  return imgPromise
}

// new Download({mode: '755'})
//     .get('http://example.com/foo.zip')
//     .get('http://example.com/cat.jpg')
//     .dest('dest')
//     .run(callback)

let Utils = {
  x: x,
  Download: Download,
  downloadImageFiles: downloadImageFiles,
  newURL: newURL,
  newJsonUrl: newJsonUrl,
  recordURL: recordURL,
  queryTitle: queryTitle,
  queryNumRecords: queryNumRecords,
  scrapeImageFiles: scrapeImageFiles,
  getImgDirectory: getImgDirectory,
  genFileName: genFileName,
  getFileExt: getFileExt,
  removeLastSlash: removeLastSlash
}
window.Utils = Utils

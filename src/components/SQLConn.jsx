'use strict'

let React = require('react')
let SQL = window.SQL
let SQLConn = React.createClass({
  getInitialState () {
    return {
      sqlError: false,
      sqlProcessing: false,
      sqlConnected: true
    }
  },
  componentDidMount () {
    //  console.log('mounted sql component')
    this.setState({sqlProcessing: true})
    let sqlPromise = SQL.beginConnection()
    sqlPromise.then((val) => {
      if (val) {
        logger.info(`Connected to SQL Database`)
        this.setState({sqlProcessing: false, sqlConnected: true, sqlError: false})
      } else {
        logger.error('Not Connected to SQL Database')
        this.setState({sqlProcessing: false, sqlConnected: false, sqlError: true})
      }
    })
    .catch(() => {
      logger.error('Not Connected to SQL Database')
      this.setState({sqlProcessing: false, sqlConnected: false, sqlError: true})
    })
  },
  componentWillUnmount () {
    SQL.connection.end()
  },
  render () {
    const sqlConn = 'SQL Connection - '
    const buttonBaseClass = 'button-COLOR button-outline'
    let buttonClass = 'button button-outline'
    let sqlMessage = 'SQL Connection - ?'
    if (this.state.sqlProcessing) {
      sqlMessage = sqlConn + 'Processing'
      buttonClass = buttonBaseClass.replace('COLOR', 'yellow')
    } else if (this.state.sqlError) {
      sqlMessage = sqlConn + 'Error'
      buttonClass = buttonBaseClass.replace('COLOR', 'red')
    } else if (this.state.sqlConnected) {
      sqlMessage = sqlConn + 'Good'
      buttonClass = buttonBaseClass.replace('COLOR', 'green')
    }
    return (
      <button disabled className={buttonClass}>{sqlMessage} {this.state.sqlProcessing ? <i className='fa fa-spinner fa-spin'></i> : ''}</button>
    )
  }
})
module.exports = SQLConn

'use strict'
/**
 * @menu.js
 * This file builds the menus items
 *
 */

const remote = require('electron').remote
const Menu = remote.Menu

let template = [
  {
    label: 'View',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click: function (item, focusedWindow) {
          if (focusedWindow) {
            focusedWindow.reload()
          }
        }
      },
      {
        label: 'Toggle Developer Tools',
        accelerator: (function () {
          if (process.platform === 'darwin') {
            return 'Alt+Command+I'
          } else {
            return 'Ctrl+Shift+I'
          }
        })(),
        click: function (item, focusedWindow) {
          if (focusedWindow) {
            focusedWindow.toggleDevTools()
          }
        }
      }
    ]
  },
  {
    label: 'Window',
    role: 'window',
    submenu: [
      {
        label: 'Minimize',
        accelerator: 'CmdOrCtrl+M',
        role: 'minimize'
      },
      {
        label: 'Close',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      }
    ]
  },
  {
    label: 'Scraper',
    submenu: [
      {
        label: 'Log Everything',
        type: 'checkbox',
        click: function (item, focusedWindow) {
          if (item.checked) {
            logger.transports.file.level = 'info'
          } else {
            logger.transports.file.level = 'error'
          }
        }
      }
    ]
  }
]
let menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)

# Electron Scraper

My client wanted a desktop client website scraper that could handle processing hundred of thousands of records.   The scraping included going to a target sight specified by a URL and begin scraping and downloading text and images for a record.  These records would then need to be uploaded to an existing production MySQL database and Linux server.  

The project was a huge success. Here are some great feature of the applications:
1. Robust Error Handling and Logging
2. Live Progress Report
3. Clean User Interface (UI)
4. Cross Platform (Linux, MacOS, Windows)  


Here are some details of the desktop client itself:
* Written in JavaScript using Electron (http://electron.atom.io/)
* Uses cutting edge JavaScript, ES2015 and ES2016  
* UI is coded in React (https://facebook.github.io/react/)  
* Minimal Styling with Milligram.css (https://milligram.github.io)

## How to start
How to start in Development:
1. `git clone https://jeremybyu@bitbucket.org/jeremybyu/electron-scraper.git`
1. `cd electron-scraper`
1. `npm install`
1. `npm start`

Transpilation of ES6/ES7 to ES5:
npm run watch

## Notes
This code needs a SQL database connection to actually scrape. You will see errors if none exists.

Log files are placed in folder log/
Images are placed in folder images/

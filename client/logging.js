'use strict'
/**
 * @logging.js
 * This file allows loading the winston NPM module for logging
 * The result is stored in the global variable logger
 */

let winston = require('winston')
let fs1 = require('fs')

var logDir = 'logs' // directory path you want to set
if (!fs1.existsSync(logDir)) {
  // Create the directory if it does not exist
  fs1.mkdirSync(logDir)
}

let logger = new winston.Logger({
  transports: [
    new winston.transports.File({ filename: `./${logDir}/scraper.log`, level: 'error' })
  ],
  exitOnError: false
})

// const remote = require('electron').remote
// const Menu = remote.Menu
// const MenuItem = remote.MenuItem
//
// var menu = new Menu()
// menu.append(new MenuItem({ label: 'MenuItem1', click: function () { console.log('item 1 clicked') } }))
// Menu.setApplicationMenu(menu)
//  winston.add(winston.transports.File, { filename: `./${logDir}/scraper.log` })

window.logger = logger

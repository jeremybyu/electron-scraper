'use strict'
let Menu = require('menu')
const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600})

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/index.html')

  // Open the DevTools.
  //  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', createWindow)
//  This will build the menu
app.once('ready', () => {
  // let template = [
  //   {
  //     label: 'View',
  //     submenu: [
  //       {
  //         label: 'Reload',
  //         accelerator: 'CmdOrCtrl+R',
  //         click: function (item, focusedWindow) {
  //           if (focusedWindow) {
  //             focusedWindow.reload()
  //           }
  //         }
  //       },
  //       {
  //         label: 'Toggle Developer Tools',
  //         accelerator: (function () {
  //           if (process.platform === 'darwin') {
  //             return 'Alt+Command+I'
  //           } else {
  //             return 'Ctrl+Shift+I'
  //           }
  //         })(),
  //         click: function (item, focusedWindow) {
  //           if (focusedWindow) {
  //             focusedWindow.toggleDevTools()
  //           }
  //         }
  //       }
  //     ]
  //   },
  //   {
  //     label: 'Window',
  //     role: 'window',
  //     submenu: [
  //       {
  //         label: 'Minimize',
  //         accelerator: 'CmdOrCtrl+M',
  //         role: 'minimize'
  //       },
  //       {
  //         label: 'Close',
  //         accelerator: 'CmdOrCtrl+W',
  //         role: 'close'
  //       }
  //     ]
  //   }
  // ]
  // let menu = Menu.buildFromTemplate(template)
  // Menu.setApplicationMenu(menu)
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})
